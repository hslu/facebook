HSLU Facebook Demo
========================

1) Installation on Ubuntu 14.10
----------------------------------

### Install dependencies

    apt-get install git php5 php5-curl curl

### Clone project

    git clone https://bitbucket.org/hslu/facebook.git
    
### Load composer into project

    cd /path/to/project/dir
    curl -sS https://getcomposer.org/installer | php

### Get depending php packages

    php composer.phar install

### Configure additional parameters
Composer asks you for some additional parameters.
You just need to set the facebook appid and appsecret.

### Run application on integrated webserver

    php app/console server:run