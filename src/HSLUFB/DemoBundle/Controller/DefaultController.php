<?php

namespace HSLUFB\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequestException;
use Facebook\FacebookRequest;
use Facebook\GraphUser;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $facebookSession = null;
        $userProfile = null;

        $localUserSession = new Session();

        $appid =  $this->container->getParameter('fbAppID');
        $appsecret = $this->container->getParameter('fbAppSecret');

        FacebookSession::setDefaultApplication($appid, $appsecret);

        $helper = new FacebookRedirectLoginHelper('http://localhost:8000/');

        if ($localUserSession->get('fbsession')) {
            $facebookSession = $localUserSession->get('fbsession');
        } else {
            try {
                $facebookSession = $helper->getSessionFromRedirect();
            } catch(FacebookRequestException $ex) {
                // When Facebook returns an error
                echo $ex->getMessage();
            } catch(\Exception $ex) {
                // When validation fails or other local issues
                echo $ex->getMessage();
            }
        }
        $loginUrl = $helper->getLoginUrl(array('scope' => 'user_friends, email,user_photos,user_about_me'));
        if ($facebookSession) {
            $localUserSession->set('fbsession',$facebookSession);
            try {
                $userProfile = (new FacebookRequest(
                    $facebookSession, 'GET', '/me'
                ))->execute()->getGraphObject();

                $user_picture = (new FacebookRequest(
                    $facebookSession, 'GET', '/me/picture'
                ))->execute()->getGraphObject();
                var_dump($user_picture);
            } catch(FacebookRequestException $e) {
                echo "Exception occured, code: " . $e->getCode();
                echo " with message: " . $e->getMessage();

            }
            if ($userProfile) {
                return $this->render('HSLUFBDemoBundle:Default:profile.html.twig', array('profile' => $userProfile, 'picture' => $user_picture));
            } else {
                return $this->render('HSLUFBDemoBundle:Default:index.html.twig', array('loginurl' => $loginUrl));
            }
        } else {
            return $this->render('HSLUFBDemoBundle:Default:index.html.twig', array('loginurl' => $loginUrl));
        }
    }

    public function logoutAction() {
        $session = new Session();
        $session->set('fbsession',null);
        return $this->redirect('/');
    }
}
